<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190528125906 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE exemplaar (id INT AUTO_INCREMENT NOT NULL, id_film_id INT NOT NULL, gebruiker_id INT DEFAULT NULL, nummer INT NOT NULL, aanwezig SMALLINT NOT NULL, UNIQUE INDEX UNIQ_E17666339DCC5869 (nummer), INDEX IDX_E176663388E2F8F3 (id_film_id), INDEX IDX_E17666339C92A3DF (gebruiker_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE film (id INT AUTO_INCREMENT NOT NULL, titel VARCHAR(255) NOT NULL, lidwoord VARCHAR(255) DEFAULT NULL, imdb_movie_id VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gebruiker (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, naam VARCHAR(255) NOT NULL, voornaam VARCHAR(255) NOT NULL, telefoon VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_89DCDB1FE7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exemplaar ADD CONSTRAINT FK_E176663388E2F8F3 FOREIGN KEY (id_film_id) REFERENCES film (id)');
        $this->addSql('ALTER TABLE exemplaar ADD CONSTRAINT FK_E17666339C92A3DF FOREIGN KEY (gebruiker_id) REFERENCES gebruiker (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exemplaar DROP FOREIGN KEY FK_E176663388E2F8F3');
        $this->addSql('ALTER TABLE exemplaar DROP FOREIGN KEY FK_E17666339C92A3DF');
        $this->addSql('DROP TABLE exemplaar');
        $this->addSql('DROP TABLE film');
        $this->addSql('DROP TABLE gebruiker');
    }
}
