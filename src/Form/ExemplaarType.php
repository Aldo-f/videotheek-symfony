<?php

namespace App\Form;

use App\Entity\Exemplaar;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use PhpParser\Node\Stmt\Label;

class ExemplaarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // https://symfony.com/doc/current/form/dynamic_form_modification.html
        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $exemplaar = $event->getData();
                $form = $event->getForm();
                $form->add('nummer')
                    ->add('idFilm');
                if (!$exemplaar || null === $exemplaar->getId()) {
                    // Don't create aanwezig if Exemplaar is new
                    // create
                } else {
                    // update
                    $form->add('aanwezig', ChoiceType::class, [
                        'choices' => [
                            "Aanwezig" => 1,
                            "Uitgeleend" => 0,
                            "Onbekend" => -1
                        ]
                    ])
                        ->add('gebruiker');
                }
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Exemplaar::class,
        ]);
    }
}