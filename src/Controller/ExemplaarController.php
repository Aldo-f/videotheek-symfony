<?php

namespace App\Controller;

use App\Entity\Exemplaar;
use App\Form\ExemplaarType;
use App\Repository\ExemplaarRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/exemplaar")
 */
class ExemplaarController extends AbstractController
{
    /**
     * @Route("/", name="exemplaar_index", methods={"GET"})
     */
    public function index(ExemplaarRepository $exemplaarRepository): Response
    {
        return $this->render('exemplaar/index.html.twig', [
            // Gesorteerd op nummer
            'exemplaars' => $exemplaarRepository->findBy(array(), array('nummer' => 'asc')),
        ]);
    }

    /**
     * @Route("/new", name="exemplaar_new", methods={"GET","POST"}) 
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        // Todo: plaats idFilm al klaar wanneer nieuw exemplaar uit show_film komt

        $exemplaar = new Exemplaar();
        $form = $this->createForm(ExemplaarType::class, $exemplaar);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $exemplaar->setAanwezig(1);
            $entityManager->persist($exemplaar);
            $entityManager->flush();

            return $this->redirectToRoute('exemplaar_index');
        }

        // Access based on role
        // $this->denyAccessUnlessGranted('ROLE_ADMIN');

        return $this->render('exemplaar/new.html.twig', [
            'exemplaar' => $exemplaar,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="exemplaar_show", methods={"GET"})
     */
    public function show(Exemplaar $exemplaar): Response
    {
        return $this->render('exemplaar/show.html.twig', [
            'exemplaar' => $exemplaar,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="exemplaar_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Exemplaar $exemplaar): Response
    {
        $form = $this->createForm(ExemplaarType::class, $exemplaar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('exemplaar_index', [
                'id' => $exemplaar->getId(),
            ]);
        }

        return $this->render('exemplaar/edit.html.twig', [
            'exemplaar' => $exemplaar,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="exemplaar_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Exemplaar $exemplaar): Response
    {
        if ($this->isCsrfTokenValid('delete' . $exemplaar->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($exemplaar);
            $entityManager->flush();
        }

        return $this->redirectToRoute('exemplaar_index');
    }
}