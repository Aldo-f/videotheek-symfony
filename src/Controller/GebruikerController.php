<?php

namespace App\Controller;

use App\Repository\ExemplaarRepository;
use App\Repository\GebruikerRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gebruiker")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class GebruikerController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(ExemplaarRepository $exemplaarRepository)
    {
        $user = $this->getUser();
        $exemplaren = $exemplaarRepository->findByUserId($user->getId());

        return $this->render('gebruiker/index.html.twig', [
            'user' => $user,
            'exemplaren' => $exemplaren
        ]);
    }
    /**
     * @Route("/lijst")
     * @IsGranted("ROLE_ADMIN")
     */
    public function alleGebruikers(GebruikerRepository $gebruikerRepository)
    {
        return $this->render('gebruiker/table.html.twig', [
            'gebruikers' => $gebruikerRepository->findBy(array(), array('naam' => 'asc')),
        ]);
    }
    /**
     * @Route("/{id}", name="gebruiker_show", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function show(GebruikerRepository $gebruikerRepository, $id)
    {
        return $this->render('gebruiker/index.html.twig', [
            'user' => $gebruikerRepository->findOneBy(["id" => $id]),
        ]);
    }
}