<?php

namespace App\Controller;

use App\Entity\Film;
use App\Entity\Exemplaar;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    public function showFilms($films)
    {
        $filmsExemplaren = [];
        foreach ($films as $key => $film) {
            $id = $film->getId();

            $exemplaren = $this->getDoctrine()
                ->getRepository(Exemplaar::class)
                ->findBy(
                    ['idFilm' => $id]
                );
            array_push($filmsExemplaren, [$film, $exemplaren]);
        }
        return $filmsExemplaren;
    }
    /**
     * @Route("/search", name="search")
     */
    public function index()
    {
        return $this->render('search/index.html.twig', [
            'controller_name' => 'SearchController',
        ]);
    }
    public function searchBar()
    {
        $form = $this->createFormBuilder(null)
            ->setAction($this->generateUrl('film_search'))
            ->add('query', TextType::class, [
                'attr' => [
                    'class' => "ui icon input"
                ]
            ])
            ->add(
                'search',
                SubmitType::class,
                [
                    'attr' => [
                        'class' => "ui button"
                    ], 'label' => 'Zoek'
                ]
            )
            ->getForm();

        return $this->render('search/searchBar.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/zoek/{query}", name="film_search")
     */
    public function searchExemplaar($query = null, Request $request): Response
    {
        // haal waarde op uit url of uit response
        if ($query != null) {
            // echo "Uit url<br>";
        } else {
            // echo "uit response:<br> ";
            $query = $request->request->get('form')['query'];
        }

        $filmRepository = $this->getDoctrine()->getRepository(Film::class);

        if (is_numeric($query)) {
            // echo "getal " . $query;
            // haal exemplaar op
            $exemplaarRepository = $this->getDoctrine()->getRepository(Exemplaar::class);
            $exemplaar = $exemplaarRepository->findOneBy(
                ["nummer" => $query]
            );
            // TODO: maak exception
            if (!$exemplaar) {
                // geen exemplaar gevonden
                return $this->render('film/index.html.twig', [
                    'filmsExemplaren' => null
                ]);
            }
            // haal film op
            $filmId = $exemplaar->getIdFilm()->getId();
            $film = $filmRepository->findBy(["id" => $filmId]);
        } else {
            // haal fluffy films op
            // echo "string " . $query;
            $film = $filmRepository->fluffySearch($query);
        }

        // maak film + alle exemplaren
        $filmsExemplaren = self::showFilms($film);

        // TODO: toon query als search input field
        return $this->render('film/index.html.twig', [
            'filmsExemplaren' => $filmsExemplaren, 
            'query' => $query
        ]);

        // return $this->render('debug/index.html.twig', [
        //     'debug' => $filmsExemplaren
        //     // 'filmsExemplaren' => $filmsExemplaren
        // ]);
    }
}