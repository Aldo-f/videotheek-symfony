<?php

namespace App\Controller;

use App\Entity\Film;
use App\Form\FilmType;
use App\Entity\Exemplaar;
use App\Repository\FilmRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/film")
 */
class FilmController extends AbstractController
{
    public function showFilms($films)
    {
        $filmsExemplaren = [];
        foreach ($films as $film) {
            $id = $film->getId();

            $exemplaren = $this->getDoctrine()
                ->getRepository(Exemplaar::class)
                ->findBy(
                    ['idFilm' => $id],
                    // Sorteer ze
                    array('nummer' => 'asc')
                );
            array_push($filmsExemplaren, [$film, $exemplaren]);
        }
        return $filmsExemplaren;
    }

    /**
     * @Route("/", name="film_index", methods={"GET"})
     */
    public function index(FilmRepository $filmRepository): Response
    {
        // $films = $filmRepository->findAllAsc();
        // $filmsExemplaren = self::showFilms($films);

        return $this->render('film/index.html.twig', [
            // 'filmsExemplaren' => $filmsExemplaren
            'filmsExemplaren' => self::showFilms($filmRepository->findAllAsc())
        ]);
    }

    /**
     * @Route("/new", name="film_new", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        $film = new Film();
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($film);
            $entityManager->flush();

            return $this->redirectToRoute('film_index');
        }

        return $this->render('film/new.html.twig', [
            'film' => $film,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="film_show", methods={"GET"})
     */
    public function show(Film $film): Response
    {
        // haal info op op basis van imdb_movie_id
        $url = 'http://www.omdbapi.com/?i=' . $film->getImdbMovieId() . '&apikey=' . $_ENV["OMDB_KEY"]; // path to your JSON file
        $data = file_get_contents($url); // put the contents of the file into a variable
        $json = json_decode($data, true); // decode the JSON feed     

        // Toont alle exemplaren van film
        $exemplaarRepository = $this->getDoctrine()->getRepository(Exemplaar::class);
        $exemplaar = $exemplaarRepository->findBy(
            ["idFilm" => $film->getId()]
        );

        return $this->render('film/show.html.twig', [
            'film' => $film,
            'json' => $json,
            'exemplaar' => $exemplaar

        ]);
    }

    /**
     * @Route("/{id}/edit", name="film_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Film $film): Response
    {
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('film_index', [
                'id' => $film->getId(),
            ]);
        }

        return $this->render('film/edit.html.twig', [
            'film' => $film,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="film_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Film $film): Response
    {
        if ($this->isCsrfTokenValid('delete' . $film->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($film);
            $entityManager->flush();
        }

        return $this->redirectToRoute('film_index');
    }
}