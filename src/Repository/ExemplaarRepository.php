<?php

namespace App\Repository;

use App\Entity\Exemplaar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Exemplaar|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exemplaar|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exemplaar[]    findAll()
 * @method Exemplaar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExemplaarRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Exemplaar::class);
    }

    /**
     * @return Exemplaar[] Returns an array of Exemplaar objects
     */
    public function findByUserId($id)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.gebruiker = :val')
            ->setParameter('val', $id)
            ->orderBy('e.id', 'ASC')
            // ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }


    // /**
    //  * @return Exemplaar[] Returns an array of Exemplaar objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Exemplaar
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}