<?php

namespace App\Repository;

use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Film::class);
    }

    public function findAllAsc()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.titel', 'ASC')
            ->getQuery()
            ->execute();
    }

    /**
     * @return Film[] Returns an array of Film objects
     */
    public function fluffySearch($value)
    {
        // make it so fluffy i'm gonna die
        $value = "%" . $value . "%";

        $qb = $this->createQueryBuilder('u');
        $qb->where(
            $qb->expr()->like('u.titel', ':titel')
        )->setParameter('titel', $value);

        return $qb->getQuery()->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Film
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}