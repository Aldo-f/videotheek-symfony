<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

// To use UniqueEntity in Annotations
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilmRepository")
 * @UniqueEntity("titel", message="Elke film moet een unieke titel hebben")
 */
class Film
{
    /**
     * Generates the magic method!!
     * for labels  
     */
    public function __toString()
    {
        // to show the titel of the Exemplaar in the select
        return $this->titel;
        // return $this->lidwoord;
        // return $this->imdb_movie_id;
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lidwoord;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imdb_movie_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Exemplaar", mappedBy="idFilm", orphanRemoval=true)
     */
    private $exemplaren;

    public function __construct()
    {
        $this->exemplaren = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitel(): ?string
    {
        return $this->titel;
    }

    public function setTitel(string $titel): self
    {
        $this->titel = $titel;

        return $this;
    }

    public function getLidwoord(): ?string
    {
        return $this->lidwoord;
    }

    public function setLidwoord(?string $lidwoord): self
    {
        $this->lidwoord = $lidwoord;

        return $this;
    }

    public function getImdbMovieId(): ?string
    {
        return $this->imdb_movie_id;
    }

    public function setImdbMovieId(?string $imdb_movie_id): self
    {
        $this->imdb_movie_id = $imdb_movie_id;

        return $this;
    }

    /**
     * @return Collection|Exemplaar[]
     */
    public function getExemplaren(): Collection
    {
        return $this->exemplaren;
    }

    public function addExemplaren(Exemplaar $exemplaren): self
    {
        if (!$this->exemplaren->contains($exemplaren)) {
            $this->exemplaren[] = $exemplaren;
            $exemplaren->setIdFilm($this);
        }

        return $this;
    }

    public function removeExemplaren(Exemplaar $exemplaren): self
    {
        if ($this->exemplaren->contains($exemplaren)) {
            $this->exemplaren->removeElement($exemplaren);
            // set the owning side to null (unless already changed)
            if ($exemplaren->getIdFilm() === $this) {
                $exemplaren->setIdFilm(null);
            }
        }

        return $this;
    }
}