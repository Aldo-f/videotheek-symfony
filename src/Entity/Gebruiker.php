<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GebruikerRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class Gebruiker implements UserInterface
{
    /**
     * Generates the magic method!!
     * for labels  
     */
    public function __toString()
    {
        // to show the email of the Gebruiker in the select
        // return $this->id;
        // return $this->naam;
        // return $this->voornaam;
        return $this->email;
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $naam;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $voornaam;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefoon;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Exemplaar", mappedBy="gebruiker")
     */
    private $idExemplaar;

    public function __construct()
    {
        $this->idExemplaar = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNaam(): ?string
    {
        return $this->naam;
    }

    public function setNaam(string $naam): self
    {
        $this->naam = $naam;

        return $this;
    }

    public function getVoornaam(): ?string
    {
        return $this->voornaam;
    }

    public function setVoornaam(string $voornaam): self
    {
        $this->voornaam = $voornaam;

        return $this;
    }

    public function getTelefoon(): ?string
    {
        return $this->telefoon;
    }

    public function setTelefoon(?string $telefoon): self
    {
        $this->telefoon = $telefoon;

        return $this;
    }

    /**
     * @return Collection|Exemplaar[]
     */
    public function getIdExemplaar(): Collection
    {
        return $this->idExemplaar;
    }

    public function addIdExemplaar(Exemplaar $idExemplaar): self
    {
        if (!$this->idExemplaar->contains($idExemplaar)) {
            $this->idExemplaar[] = $idExemplaar;
            $idExemplaar->setGebruiker($this);
        }

        return $this;
    }

    public function removeIdExemplaar(Exemplaar $idExemplaar): self
    {
        if ($this->idExemplaar->contains($idExemplaar)) {
            $this->idExemplaar->removeElement($idExemplaar);
            // set the owning side to null (unless already changed)
            if ($idExemplaar->getGebruiker() === $this) {
                $idExemplaar->setGebruiker(null);
            }
        }

        return $this;
    }
}