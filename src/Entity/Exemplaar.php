<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

// To use UniqueEntity in Annotations
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExemplaarRepository")
 * @UniqueEntity("nummer", message="Elk exemplaar moet een uniek nummer hebben")
 */
class Exemplaar
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $nummer;

    /**
     * @ORM\Column(type="smallint")
     */
    private $aanwezig;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Film", inversedBy="exemplaren")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idFilm;

    // /**
    //  * @ORM\ManyToOne(targetEntity="App\Entity\Gebruiker", inversedBy="exemplaren")
    //  */
    // private $idGebruiker;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Gebruiker", inversedBy="idExemplaar")
     */
    private $gebruiker;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNummer(): ?int
    {
        return $this->nummer;
    }

    public function setNummer(int $nummer): self
    {
        $this->nummer = $nummer;

        return $this;
    }

    public function getAanwezig(): ?int
    {
        return $this->aanwezig;
    }

    public function setAanwezig(int $aanwezig): self
    {
        $this->aanwezig = $aanwezig;

        return $this;
    }

    public function getIdFilm(): ?Film
    {
        return $this->idFilm;
    }

    public function setIdFilm(?Film $idFilm): self
    {
        $this->idFilm = $idFilm;

        return $this;
    }

    // public function getIdGebruiker(): ?Gebruiker
    // {
    //     return $this->idGebruiker;
    // }

    // public function setIdGebruiker(?Gebruiker $idGebruiker): self
    // {
    //     $this->idGebruiker = $idGebruiker;

    //     return $this;
    // }

    public function getGebruiker(): ?Gebruiker
    {
        return $this->gebruiker;
    }

    public function setGebruiker(?Gebruiker $gebruiker): self
    {
        $this->gebruiker = $gebruiker;

        return $this;
    }
}