// alert("main.js");

// @ts-ignore
$('.ui.form')
    .form({
        fields: {
            titel: 'empty',
            lidwoord: ['minLength[6]', 'empty'],
            imdb_movie_id: 'empty',
            password: ['minLength[6]', 'empty'],
            skills: ['minCount[2]', 'empty'],
            terms: 'checked'
        }
    });

$('.message .close')
    .on('click', function () {
        $(this)
            .closest('.message')
            .transition('fade');
    });

$('.ui.dropdown')
    .dropdown();