# Videotheek
<p>
Extra opdracht PHP MVC.
</p>
Als webontwikkelaar wordt jou gevraagd een webapplicatie te schrijven die de medewerkers in staat stelt deze filmcollectie te beheren. 

## Basismogelijkheden
* [x] ``Filmoverzicht``, alfabetisch per titel

| titel            | Nummer(s)                         | Exemplaren aawezig |
| ---------------- | --------------------------------- | ------------------ |
| Descendants, The | <strong>5 6</strong>              | 2                  |
| Trespass         | <strong> 120 121 123 </strong>141 | 3                  |


* [x] ``Zoeken op nummer``, ingeven van nummer 65 geeft volgend resultaat:

| titel        | Nummer(s)                    | Exemplaren aawezig |
| ------------ | ---------------------------- | ------------------ |
| Darkest Hour | 65 <strong>  66 </strong> 98 | 1                  |

* [x] Invoeren:
  * [x] Nieuwe ``titel``; foutmelding indien al bestaat; maakt geen exemplaren aan
  * [x] Nieuw exemplaar; keuzelijst van alle titels (met zoekveld), ``exemplaarNummer`` (is dus geen id maar een uniek nummer), foutmelding indien nummer reeds bestaat; nieuw exemplaar is initieel aanwezig
* [x] Verwijderen:
  * [x] Van een titel: Titel uit keuezelijst om te verwijderen; verwijderd automatisch alle exemplaaren
  * [x] Knop bij ``Bekijk``
* [x] Update:
  * [x] Huren van een film (plaats ``exemplaarNummer`` als uitgeleend of beschikbaar = 0 )
  * [x] Terugbrengen van een film; plaats het nummer terug beschikbaar, komt weer <strong>bold</strong> te staan 

Bovenstaande is beschikbaar via een hoofdmenu

## Uitbreding
* [ ] Lijst van gebruikers en hun wachtwoorden (SHA512)
* [ ] Bediende moet eerst ingelogd zijn om aanpassingen te kunnen voltooien
* [ ] Klanten kunnen hun uitgeleende items bekijken


---------------
# Uitwerking
* [x] Maak schema en DB
* [x] Gebruik IMDB 
* [x] Maak Symfony-project
* [ ] Maak Laravel-project


### Model voor DB, EER-diagram


| Film                         | Exemplaar                    | Gebruiker                    |
| ---------------------------- | ---------------------------- | ---------------------------- |
| <u>idFilm</u> (int, AI)      | <u>idExemplaar</u> (int, AI) | <u>idGebruiker</u> (int, AI) |
| titel (VARSCHAR(140))        | idFilm                       | naam                         |
| lidwoord (VARSCHAR(15))      | nummer (int)                 | voornaam                     |
| imdb_movie_id (VARSCHAR(45)) | aanwezig (tinyint)           | email                        |
|                              |                              | wachtwoord                   |
 
 aanwezig  = 1; uitgeleend = 0; onbekend = -1


### IMDB API
[IMDB](https://api.themoviedb.org/3/movie/tt3896198?api_key=d21283b34416ede4cf71abdc3e0f4672&language=nl-BE) 

### OMDb API
[OMDb](http://www.omdbapi.com/?i=tt3896198&apikey=f71b15ab)


## Commands Symfony
### Backend
```
composer require symfony/orm-pack
composer require --dev symfony/maker-bundle
composer require symfony/debug-bundle
composer require form validator twig-bundle security-csrf annotations
php bin/console doctrine:database:create
php bin/console make:entity
php bin/console make:migration 
php bin/console doctrine:migrations:migrate
php bin/console make:crud
```
![CRUD Symfony](https://i.imgur.com/KjsO2Kw.jpg)<br>

### Frontend
```
# terminal
composer require encore
yarn install
# compile assets 
yarn encore dev
yarn encore dev --watch
yarn encore production
# after app.js changes
yarn add jquery
```
```
// app.js
const $ = require('jquery');
require('main.js');
```
```
{# base.html.twig #}
{# style #}
{% block stylesheets %}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.css" rel="stylesheet"/>
    {{ encore_entry_link_tags('app') }}
{% endblock %}
{# body block and wrap #}
<div class="ui container">
    {% block body %}{% endblock %}
</div>
{# js-block #}
{% block javascripts %}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js"></script>
    {{ encore_entry_script_tags('app') }}
{% endblock %}
```

## Commands Laravel
```
# terminal 
composer global require laravel/installer
laravel new laravel_videotheek
```

-------------------------------
## Lokaal starten
### Benodigdheden
* [Lando](https://lando.dev)
* [composer without server](https://www.jeffgeerling.com/blog/2018/installing-php-7-and-composer-on-windows-10)
* [Node.JS](https://nodejs.org/en/)

### Terminal
```
composer i && npm i
lando start
lando db-import /.bin/mysql.sql
```