-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2019 at 07:11 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aldofieuw_videotheek_symfony_2`
--

-- --------------------------------------------------------

--
-- Table structure for table `exemplaar`
--

CREATE TABLE `exemplaar` (
  `id` int(11) NOT NULL,
  `id_film_id` int(11) NOT NULL,
  `gebruiker_id` int(11) DEFAULT NULL,
  `nummer` int(11) NOT NULL,
  `aanwezig` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exemplaar`
--

INSERT INTO `exemplaar` (`id`, `id_film_id`, `gebruiker_id`, `nummer`, `aanwezig`) VALUES
(1, 1, NULL, 5, 0),
(2, 1, NULL, 10, 1),
(3, 1, NULL, 4, 0),
(5, 1, 1, 1, 0),
(6, 2, NULL, 3, 1),
(8, 1, 1, 2, 0),
(15, 10, NULL, 8, -1),
(18, 4, 3, 6, 0),
(21, 9, NULL, 20, 1),
(23, 13, NULL, 85, 0),
(24, 16, NULL, 64, 1),
(25, 8, NULL, 98, 1),
(26, 15, NULL, 12365, 1);

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `titel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lidwoord` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imdb_movie_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `titel`, `lidwoord`, `imdb_movie_id`) VALUES
(1, 'Booksmart', NULL, 'tt1489887'),
(2, 'Welcome to Marwen', NULL, 'tt3289724'),
(3, 'Shawshank Redemption', 'The', 'tt0111161'),
(4, 'Godfather', 'The', 'tt0068646'),
(8, 'Funny Story', NULL, 'tt7422552'),
(9, 'Godfather: Part II', 'The', 'tt0071562'),
(10, 'Godfather: Part III', 'The', 'tt0099674'),
(12, 'Ex Machina', NULL, 'tt0470752'),
(13, 'Film zonder imdb movie id', NULL, NULL),
(14, 'Fluffy', NULL, 'tt0059188'),
(15, 'Life of fluffy Oswald', 'The', 'drfgthyjukillopm'),
(16, 'Oswald the Lucky Rabbit Greeting Card', NULL, 'tt3502824');

-- --------------------------------------------------------

--
-- Table structure for table `gebruiker`
--

CREATE TABLE `gebruiker` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `naam` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voornaam` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefoon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gebruiker`
--

INSERT INTO `gebruiker` (`id`, `email`, `roles`, `password`, `naam`, `voornaam`, `telefoon`) VALUES
(1, 'aldo@getnada.com', '[\"ROLE_ADMIN\"]', '$argon2i$v=19$m=1024,t=2,p=2$eGsvT1lpNDVEd0Q2LzR2UQ$sZ8e8ripVypGTj6LzVRC1RwuFG0aFOmzC7KfsMuFCvY', 'Fieuw', 'Aldo', '0494157842'),
(2, 'pol@getnada.com', '[]', '$argon2i$v=19$m=1024,t=2,p=2$Z2RBS2tUekJkOVB0WDNsRw$hvnU2El0lmdgUz+th1Q7HCTWYtjgEgKYXPT1nKKO9MU', 'Tremerie', 'Pol', '0494785794'),
(3, 'adinda.mattens@vdab.be', '[\"ROLE_ADMIN\"]', '$argon2i$v=19$m=1024,t=2,p=2$djdlbGJVMGxnM1NkdnpPMA$OTpm86UOgm4JRIP0yDYXyMqMgYcGE5127xyyMNE78Dw', 'Mattens', 'Adinda', NULL),
(4, 'Boelie@prot.be', '[\"ROLE_ADMIN\"]', '$argon2i$v=19$m=1024,t=2,p=2$eVVYUy5KSXRlSTlaamtrMQ$o8zVzhVzqKTQmN8QhkX1L0CAmCrJJRG2kcmobqTcfHQ', 'Boelie', 'Prot', '0494446762');

-- --------------------------------------------------------

--
-- Table structure for table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190528125906', '2019-05-28 12:59:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exemplaar`
--
ALTER TABLE `exemplaar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_E17666339DCC5869` (`nummer`),
  ADD KEY `IDX_E176663388E2F8F3` (`id_film_id`),
  ADD KEY `IDX_E17666339C92A3DF` (`gebruiker_id`);

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gebruiker`
--
ALTER TABLE `gebruiker`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_89DCDB1FE7927C74` (`email`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exemplaar`
--
ALTER TABLE `exemplaar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `gebruiker`
--
ALTER TABLE `gebruiker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `exemplaar`
--
ALTER TABLE `exemplaar`
  ADD CONSTRAINT `FK_E176663388E2F8F3` FOREIGN KEY (`id_film_id`) REFERENCES `film` (`id`),
  ADD CONSTRAINT `FK_E17666339C92A3DF` FOREIGN KEY (`gebruiker_id`) REFERENCES `gebruiker` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
